
#include <stdio.h>
#include <stdlib.h>

#include "libmath.h"

int main(int argc, char **argv) {

	int sum, num1, num2;

	if (argc != 3) {
		printf("Usage: %s <number1> <number2>\n", argv[0]);
		return -1;
	}

	num1 = atoi(argv[1]);
	num2 = atoi(argv[2]);

	sum = add(num1, num2);

	printf("Addition = %d\n", sum);

	return 0;
}
