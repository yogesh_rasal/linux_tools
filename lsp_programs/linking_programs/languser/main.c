
#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

#include "liblang.h"

int main(int argc, char **argv)
{
	void *handle;
	char *error;
	fptrLangName pFun;

	if (argc != 3) {
		printf("Usage: %s <library_path> <fun_name>\n", argv[0]);
		return -1;
	}

	//	Open library.
	handle = dlopen(argv[1], RTLD_LAZY);
	if (!handle)
	{
		fprintf(stderr, "%s\n", dlerror());
		exit(EXIT_FAILURE);
	}

	dlerror();    /* Clear any existing error */

	pFun = (fptrLangName)dlsym(handle, argv[2]);
	if ((error = dlerror()) != NULL)
	{
		fprintf(stderr, "%s\n", error);
		exit(EXIT_FAILURE);
	}

	// Call function.
	pFun();

	dlclose(handle);
	return 0;
}
