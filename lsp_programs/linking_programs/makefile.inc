
C_COMPILER=gcc
CPP_COMPILER=g++

ARCHIEVE_UTIL=ar

DEBUGFLAGS=-g
SHARED_LIB_FLAGS = -shared -fPIC

STATIC_LIB_TARGET_DIR=~/lsp/build/static_libs
DYNAMIC_LIB_TARGET_DIR=~/lsp/build/dynalibs
BINARY_TARGET_DIR=~/lsp/build/bin
LIB_RPATH=/usr/local/lib/lsp/dynalibs

