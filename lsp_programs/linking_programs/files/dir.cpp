
#include <stdio.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>

int main(int argc, char **argv)
{
	int iRet;
	DIR *pCADirectory;
	struct dirent *pDirEntry;

	if (2 != argc)
	{
		printf("Give directory name as command line argument\n");
		return 0;
	}

	iRet = access(argv[1], F_OK);
	if (-1 == iRet)
	{
		printf("Access failed\n");
		return -1;
	}

	pCADirectory = opendir(argv[1]);
	if (NULL == pCADirectory)
	{
		perror("Directory open failed. Error:");
		return -1;
	}

	printf("\tFile Name\t File Type\n");
	printf("\t------------------------------------\n");
	while (1)
	{
		pDirEntry = readdir(pCADirectory);
		if (NULL == pDirEntry)
		{
			break;
		}

		printf("\t%s", pDirEntry->d_name);
		printf("\t\t");
		switch (pDirEntry->d_type)
		{
			case DT_BLK:  printf("block device\n");            break;
			case DT_CHR:  printf("character device\n");        break;
			case DT_DIR:  printf("directory\n");               break;
			case DT_FIFO:  printf("FIFO/pipe\n");               break;
			case DT_LNK:  printf("symlink\n");                 break;
			case DT_REG:  printf("regular file\n");            break;
			case DT_SOCK: printf("socket\n");                  break;
			default:       printf("unknown?\n");                break;
		}
	}

	closedir(pCADirectory);

	return 0;
}

