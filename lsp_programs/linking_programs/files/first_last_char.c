#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>

int main(int argc,char**argv)
{
	int fdr;
	int ret;
	char first[2];
	char last[2];
	memset(first,0,2);
	memset(last,0,2);
		
	if (argc!=2)
	{
		printf("Usage:%s <Sourcefile name>\n",argv[0]);
		return 0;
	}
	ret=access(argv[1],F_OK|R_OK);
	if(ret!=0)
	{
		printf("file is not present or do not have read permission =%s \n",argv[1]);
		return -1;
	}

	fdr=open(argv[1],O_RDONLY);
	if(-1==fdr)
	{	
		printf("file open fail_error=%s \n",argv[1]);
		return 0;
	}
	ret=read(fdr,first,1);
	printf("First character = [%s] \n",first);
	lseek(fdr,-1,SEEK_END);
	read(fdr,last,1);
	printf("Last character = [%s] \n",last);
	return 0;
}
