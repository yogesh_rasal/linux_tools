
#include<stdio.h>
#include <sys/uio.h>
#include <unistd.h>
#include <string.h>

int main()
{
	char str0[] = "hello ";
	char str1[] = "world\n";
	struct iovec iov[2];
	ssize_t nwritten;

	iov[0].iov_base = str0;
	iov[0].iov_len = strlen(str0);
	iov[1].iov_base = str1;
	iov[1].iov_len = strlen(str1);

	writev(STDOUT_FILENO, iov, 2);

	return 0;
}

