#include <unistd.h>		//to use access fun
#include <stdio.h>
#include<string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include<errno.h> 
#include <fcntl.h>
#include "lspconstant.h"
#include <stdlib.h>

int main(int argc,char **argv)
{
	if (argc != 3)
	{
		printf("Usage: %s <file_path>\n", argv[0]);		
		return 0;
	}

	int ret,fd;
	char buffer[BUFFER_SIZE+1];
	memset(buffer , 0 , sizeof(buffer));		//will set 0 in buffer 
	int size=atoi(argv[2]);
	ret = access(argv[1] ,F_OK);																						
	if(0!=ret)
	{
		printf("\nfile %s is not present ",argv[1]);		//file path
		return 0;
	}

	fd=open(argv[1],O_RDONLY);
	if(-1==fd)
	{
		printf("File open failed _Error=%s\n",strerror(errno));
		return -1;
	}
	//int length = strlen(argv[2]);
	ret=read(fd,buffer,size);
	if(ret==-1)
	{
		printf("File read failed _Error=%s\n",strerror(errno));
		close (fd);
		return -1;
	}
	printf("\ndata = %s  ",buffer);		//file path
	close(fd);

	return 0;
}
