#include<stdio.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<fcntl.h>
#include"lspconstant.h"

int main(int argc,char**argv)
{
	int fdr;
	int fdw;
	int ret;
	char buffer[BUFFER_SIZE+1];


	if (argc!=3)
	{
		printf("Usage:%s <Sourcefile name><Destination filen name>\n",argv[0]);
		return 0;
	}
	ret=access(argv[1],F_OK|R_OK);
	if(ret!=0)
	{
		printf("file is not present or do not have read permission =%s \n",argv[1]);
		return -1;
	}

	fdr=open(argv[1],O_RDONLY);
	if(-1==fdr)
	{	
		printf("file open fail_error=%s \n",argv[1]);
		return 0;
	}

	ret=access(argv[2],F_OK);
	if(ret==0)
	{
		printf("file is already present =%s \n",argv[2]);
		return -1;
	}

	fdw=open(argv[2],O_WRONLY|O_CREAT,S_IRWXU);

	if(-1==fdw)
	{	
		printf("file open fail_error=%s \n",argv[2]);
		return 0;
	}

	while(1)
	{
		ret = read(fdr,buffer,BUFFER_SIZE);
		if(ret==0)
			break;
		write(fdw,buffer,ret);
	}
	close(fdr);
	close(fdw);
	return 0;
}
