#include <unistd.h>		//to use access fun
#include <stdio.h>
int main(int argc,char **argv)
{
	if (argc != 2)
	{
		printf("Usage: %s <file_path> \n", argv[0]);		//file name
		return 0;
	}
	int ret;
	ret = access(argv[1] , F_OK);		//man access																				
	if(0!=ret)
	{
		printf("\nfile %s is not present ",argv[1]);		//file path
		return 0;
	}
		printf("\nfile %s is present ",argv[1]);			//argv[1] is file name and path of that file which we are passing 
	ret = access(argv[1] , R_OK);																						
	if(0!=ret)
	{
		printf("\nfile %s is not readable",argv[1]);

	}
	else
		printf("\nfile %s is readable ",argv[1]);
	ret = access(argv[1] , W_OK);																						
	if(0!=ret)
	{
		printf("\nfile %s is not writable",argv[1]);
		
	}
	else
		printf("\nfile %s is writable ",argv[1]);
	ret = access(argv[1] , X_OK);																						
	if(0!=ret)
	{
		printf("\nfile %s is not excutable\n",argv[1]);
		
	}
	else

		printf("\nfile %s is exceutable \n ",argv[1]);
	
	return 0;
}	
