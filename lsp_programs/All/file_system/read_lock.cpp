#include <errno.h>
#include <string.h>
#include <iostream>
#include <sys/file.h>
#include <unistd.h>
#include <stdlib.h>

#define	SLEEP_SECONDS		15

int main(int argc, char *argv[])
{
	int fd;
	int lockRes;
	struct flock fLock;					// Lock information.

	if (argc != 4)
	{
		std::cout << "Usage: " << argv[0] << " <file_path> <start_offset>, <number_of_bytes>\n";
		return -1;
	}

	fd = open(argv[1], O_RDWR);
	if (-1 == fd)
	{
		std::cout << "File open failed for error: " << strerror(errno) << std::endl;

		return -1;
	}

	fLock.l_type   = F_RDLCK;				// Acquire read lock on file.
	fLock.l_whence = SEEK_SET;				// From beginning.
	fLock.l_start  = atoi(argv[2]);			// Offset from l_whence.
	fLock.l_len    = atoi(argv[3]);			// 0 = to EOF.
	fLock.l_pid = getpid();

	std::cout << "Before acquiring read lock\n";

	lockRes = fcntl(fd, F_SETLK, &fLock);
	if (-1 == lockRes)
	{
		std::cout << "File locking failed for error: " << strerror(errno) << std::endl;
		close(fd);

		return -1;
	}

	std::cout << "After acquiring read lock\n";
	std::cout << "Sleeping for " << SLEEP_SECONDS << " seconds\n";

	sleep(SLEEP_SECONDS);

	std::cout << "After sleep call\n";

	fLock.l_type   = F_UNLCK;
	fLock.l_whence = SEEK_SET;
	fLock.l_start  = 0;
	fLock.l_len    = 0;				// length, 0 = to EOF

	lockRes = fcntl(fd, F_SETLK, &fLock);

	close(fd);

	return 0;
}

