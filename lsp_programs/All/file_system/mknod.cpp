#include<stdio.h>
#include <sys/stat.h>
#include<fcntl.h>
#include<unistd.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>

int main(int argc, char *argv[])
{
	int ret;

	//
	//	Remove directory if exists.
	//
	ret = system("rm -rf mknodtest");
	if (-1 == ret)
	{
		printf("Remove mknod directory failed for error %s\n", strerror(errno));
		return -1;
	}

	//	Creating directory.
	//	Note: mknod is not supported on some linux implementation.
	//	Hence call to mknod is commented and mkdir is used.
	//
	//ret = mknod("mknodtest", S_IFDIR | S_IRWXU | S_IWGRP | S_IXGRP, 0);
	ret = mkdir("mknodtest", S_IRWXU | S_IWGRP | S_IXGRP);
	if (-1 == ret)
	{
		printf("Directory creation failed for errno (%d) and error %s\n", errno, strerror(errno));
		return -1;
	}
	
	//
	//	Creating file.
	//	Here permissions are 777. But actual permissions will be 777 & ~(umask)
	//
	ret = mknod("mknodtest/test.txt", S_IFREG | S_IRWXU | S_IRWXG | S_IRWXO, 0);
	if (-1 == ret)
	{
		printf("File creation failed for error %s\n", strerror(errno));
		return -1;
	}

	//
	//	Creating character device.
	//
	ret = mknod("mknodtest/test.cdev", S_IFCHR | S_IRUSR | S_IWUSR | S_IRGRP, 22);
	if (-1 == ret)
	{
		printf("character device creation failed for error %s\n", strerror(errno));
		return -1;
	}

	//
	//	Creating block device.
	//
	ret = mknod("mknodtest/test.bdev", S_IFBLK | S_IRUSR | S_IWUSR | S_IRGRP, 22);
	if (-1 == ret)
	{
		printf("Block device creation failed for error %s\n", strerror(errno));
		return -1;
	}

	//
	//	Creating fifo.
	//
	ret = mknod("mknodtest/test.fifo", S_IFIFO | S_IRUSR | S_IWUSR | S_IRGRP, 0);
	if (-1 == ret)
	{
		printf("FIFO creation failed for error %s\n", strerror(errno));
		return -1;
	}

	//
	//	Creating unix socket.
	//
	ret = mknod("mknodtest/test.sock", S_IFSOCK | S_IRUSR | S_IWUSR | S_IRGRP, 0);
	if (-1 == ret)
	{
		printf("Unix socket creation failed for error %s\n", strerror(errno));
		return -1;
	}

	return 0;
}

