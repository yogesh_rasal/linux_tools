#include <errno.h>
#include <string.h>
#include <iostream>
#include <sys/file.h>
#include <unistd.h>
#include <stdlib.h>

using namespace std;

#define	SLEEP_SECONDS		15

int main(int argc, char *argv[])
{
	int fd;
	int flags;

	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " <file_path>\n";
		return -1;
	}

	fd = open(argv[1], O_RDWR);
	if (-1 == fd)
	{
		std::cout << "File open failed for error: " << strerror(errno) << std::endl;
		return -1;
	}

	flags = fcntl(fd, F_GETFL);
	if (-1 == flags) {
		std::cout << "fcntl failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	if (flags & O_RDONLY)
		cout << "File is opened in read mode\n";
	if (flags & O_WRONLY)
		cout << "File is opened in write mode\n";
	if (flags & O_RDWR)
		cout << "File is opened in read-write mode\n";

	//	Below write will override some data in file as file is not opened in append mode.
	char overrideData[] = "override";
	int ret = write(fd, overrideData, strlen(overrideData));
	if (-1 == ret) {
		std::cout << "write failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	//	Set append flag.
	flags |= O_APPEND;
	ret = fcntl(fd, F_SETFL, flags);
	if (-1 == flags) {
		std::cout << "fcntl failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	//	Below write will append some data in file.
	char append[] = "append";
	ret = write(fd, append, strlen(append));
	if (-1 == ret) {
		std::cout << "write failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	// dup behavior using fcntl.
	int newfd = fcntl(fd, F_DUPFD);
	if (-1 == newfd) {
		std::cout << "fcntl failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	//	Below write will append some data in file.
	char dupappend[] = "dupappend";
	ret = write(newfd, dupappend, strlen(dupappend));
	if (-1 == ret) {
		std::cout << "write failed for error: " << strerror(errno) << std::endl;
		close(fd);
		return -1;
	}

	close(fd);
	close(newfd);

	return 0;
}

