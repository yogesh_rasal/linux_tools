#include <stdio.h>
#include <sys/uio.h>//for iov
#include <unistd.h>
#include <string.h>
#include <fcntl.h>

int main(int iArgc, char** pArgv)
{
	char str0[] = "Anirdh ";
	char str1[] = "is a good ";
	char str2[] = "boy \n";
	struct iovec iov[3];
//	ssize_t nwritten;
    int fd;

    if( 2 !=iArgc )
    {
        printf("usage : %s <Enter a file name>",pArgv[0]);
        return -1;
    }
    
    fd = open(pArgv[1],O_WRONLY|O_CREAT|O_TRUNC,S_IRWXU);
    
	iov[0].iov_base = str0;
	iov[0].iov_len = strlen(str0);
	iov[1].iov_base = str1;
	iov[1].iov_len = strlen(str1);
	iov[2].iov_base = str2;
	iov[2].iov_len = strlen(str2);

	writev(fd, iov, 3);

    close(fd);

    memset(str0,'0',strlen(str0));
    memset(str1,'0',strlen(str1));
    memset(str2,'0',strlen(str2));
    
    fd = open(pArgv[1],O_RDONLY);
    
    readv(fd,iov,3);
   
    printf("string 1 : %s\n",str0);
    printf("string 2 : %s\n",str1);
    printf("string 3 : %s\n",str2);
    return 0;
}

