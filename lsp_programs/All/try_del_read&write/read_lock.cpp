#include <errno.h>
#include <string.h>
#include <iostream>
#include <sys/file.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define	SLEEP_SECONDS		15

int main(int argc, char *argv[])
{
	int fd;
	int lockRes;
	struct flock fLock;					// Lock information.
    char buffer[10];

	if (argc != 2)
	{
		std::cout << "Usage: " << argv[0] << " <file_path> <start_offset>, <number_of_bytes>\n";
		return -1;
	}

	fd = open(argv[1], O_RDWR);
	if (-1 == fd)
	{
		std::cout << "File open failed for error: " << strerror(errno) << std::endl;

		return -1;
	}


	std::cout << "Before acquiring read lock\n";


	sleep(SLEEP_SECONDS);

	std::cout << "After sleep call\n";

    read(fd,buffer,9);

    printf("%s",buffer);

	close(fd);

	return 0;
}

