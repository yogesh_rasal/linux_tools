#include <stdio.h>  
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()  
{  
	pid_t pid1, pid2;
	int status, ret;

	pid1 = fork();
	if(pid1 == 0)
	{  
		printf("Child1 having pid: %d is going to sleep\n", getpid());  
		sleep(10);
		printf("Child1 is wakeup and will exit with status 1\n");
		exit(1);
	}

	pid2 = fork();
	if(pid2 == 0)
	{  
		printf("Child2 having pid: %d is going to sleep\n", getpid());  
		sleep(5);
		printf("Child2 is wakeup and will exit with status 2\n");
		exit(2);
	}

	printf("Parent- Child1 pid: %u, Child2 pid: %u\n", pid1, pid2);
	ret = waitpid(-1, &status, WNOHANG);
	if (0 == ret) {
		printf("No child is terminated yet\n");
	}
	ret = waitpid(pid1, &status, 0);
	printf("waitpid() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));

	ret = waitpid(pid2, &status, 0);
	printf("waitpid() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));

	ret = waitpid(pid2, &status, 0);
	printf("waitpid() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));

	return 0;  
}

