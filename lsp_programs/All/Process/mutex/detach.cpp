#include<stdio.h>
#include<pthread.h>
#include<unistd.h>

unsigned int shared_data = 0;

void* IncrCounter(void *ptr)
{
    printf("thread : %d\n",shared_data);
    shared_data++;
	return NULL;
}

int main()
{
    int i,ret;
    for(i=0;i<90000;i++)
    {
        pthread_t tid;
        ret = pthread_create(&tid,0,&IncrCounter,0);
        printf("return value %d\n",ret);
    }
    sleep(20);
	return 0;
}

