#include<stdio.h>
#include<pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
unsigned int shared_data = 0;

void* IncrCounter(void *ptr)
{
	for (int i = 0; i < 900000; i++) {
		pthread_mutex_lock(&mutex);
		shared_data++;
		pthread_mutex_unlock(&mutex);//if number is even, do not print, release mutex
	}
	return NULL;
}

int main()
{
	pthread_t tid[2];
    pthread_create(&tid[0],0,&IncrCounter,0);
    pthread_create(&tid[1],0,&IncrCounter,0);
    sleep(1);
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);
	printf("Counter: %d\n", shared_data);
	return 0;
}

