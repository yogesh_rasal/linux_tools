#include<stdio.h>
#include<pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
unsigned int shared_data = 0;

void* evenCounter(void *ptr)
{
	for (int shared_data = 0; shared_data < 100; shared_data++) {
		pthread_mutex_lock(&mutex);
        if(shared_data%2==0)
        {
            printf("Counter even: %d\n", shared_data);
            shared_data++;
        }
		pthread_mutex_unlock(&mutex);//if number is even, do not print, release mutex
	}
	return NULL;
}

void* oddCounter(void *ptr)
{
	for (int shared_data = 0; shared_data < 100; shared_data++) {
		pthread_mutex_lock(&mutex);
        if(shared_data%2==1)
        {
            printf("Counter odd: %d\n", shared_data);
            shared_data++;
        }
		pthread_mutex_unlock(&mutex);//if number is even, do not print, release mutex
	}
	return NULL;
}

int main()
{
	pthread_t tid[2];
    pthread_create(&tid[0],0,&evenCounter,0);
    pthread_create(&tid[1],0,&oddCounter,0);
    sleep(20);
    pthread_join(tid[0],NULL);
    pthread_join(tid[1],NULL);
	return 0;
}

