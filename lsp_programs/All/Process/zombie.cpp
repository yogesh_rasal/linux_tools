#include <stdio.h>  
#include <unistd.h>
#include <stdlib.h>

int main()  
{  
	pid_t pid;  
	pid = fork();  
	if(pid == 0)  
	{
		printf("Child pid: %u, ppid: %u\n", getpid(), getppid());
		printf("Child is going to sleep\n");  
		sleep(25);
		printf("Child is wakeup\n");
		printf("Child pid: %u, ppid: %u\n", getpid(), getppid());
		exit(0);
		//return 0;
	}

	printf("Parent pid: %u, ppid: %u\n", getpid(), getppid());
	printf("Parent is going to exit\n");  

	return 0;  
}

