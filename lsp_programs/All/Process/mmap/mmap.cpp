#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <sys/mman.h>


#define	BYTES	20

int main(int argc, char *argv[])
{
	int i;
	int fd;
	int shared;
	int protection;
	char *map;
	if(argc!=4)
	{
		printf("Usage: %s <File to map> <opr: 1 read, 2 write> <shared: 1 or 0>\n:",argv[1]);
		return -1;
	}

	protection = atoi(argv[2]);
	shared = atoi(argv[3]);
	printf("File: %s, protection: %d, shared: %d\n", argv[1], protection, shared);
	fd = open(argv[1], O_RDWR);
	if (fd == -1) {
		perror("Error opening file for readwrite");
		exit(EXIT_FAILURE);
	}

	map =(char*)mmap(NULL, BYTES,
				((protection == 1) ? PROT_READ : PROT_WRITE),
				((shared == 1) ? MAP_SHARED : MAP_PRIVATE), fd, 0);
	if (map == MAP_FAILED) {
		close(fd);
		perror("Error mmapping the file");
		exit(EXIT_FAILURE);
	}

	if (protection == 1) {
		for (i = 0; i <= BYTES; ++i) {
			printf("%c", map[i]);
			fflush(stdout);
			sleep(1);
		}
		printf("\n");
	} else {
		strncpy(&map[5], "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 26);
	}
	sleep(5);

	munmap(map, BYTES);
	close(fd);
	return 0;
}

