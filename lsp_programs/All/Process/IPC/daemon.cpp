#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>
#include <time.h>
#include <signal.h>

using namespace std;
bool bExit = false;
int sleepTime = 2;

void sigIntHandler(int signum) {
	syslog(LOG_INFO, "timerd received sigint. Ignoring.....");
}

void sigTermHandler(int signum) {
	syslog(LOG_INFO, "timerd received sigterm. Terminating.....");
	bExit = true;
}

void sigHupHandler(int signum) {
	// Add code to reload configuration.
	// For time being generate random number modulo 5.
	srand (time(NULL));
	int random_number = rand() % 5;
	sleepTime = (random_number ? random_number : 1);
	syslog(LOG_INFO, "timerd received sighup. Sleep interval = %d", sleepTime);
}

void logTime(int fd){
	while (!bExit) {
		// current date/time based on current system
		time_t now = time(0);
		char* dt = ctime(&now);	// convert now to string form
		char buffer[1024] = "\0";
		snprintf(buffer, sizeof(buffer), "The local date and time is: %s", dt);
		write(fd, buffer, strlen(buffer));
		sleep(sleepTime);
	}
}   

int main(int argc, char *argv[]) {
    pid_t pid, sid;

   //Fork the Parent Process
    pid = fork();

    if (pid < 0) { exit(EXIT_FAILURE); }

    // Exit the Parent Process
    if (pid > 0) { exit(EXIT_SUCCESS); }

	printf("New process pid: %u\n", getpid());

    //Change File Mask
    umask(0);

    //Create a new Signature Id for our child
    sid = setsid();
    if (sid < 0) { exit(EXIT_FAILURE); }

    //Change Directory
    //If we cant find the directory we exit with failure.
    if ((chdir("/")) < 0) { exit(EXIT_FAILURE); }

    //Close Standard File Descriptors
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

	struct sigaction saSigInt;
	saSigInt.sa_handler = sigIntHandler;
	saSigInt.sa_flags = 0;
	sigemptyset(&saSigInt.sa_mask);
	sigaction(SIGINT, &saSigInt, NULL );

	struct sigaction saSigTerm;
	saSigTerm.sa_handler = sigTermHandler;
	saSigTerm.sa_flags = 0;
	sigemptyset(&saSigTerm.sa_mask);
	sigaction(SIGTERM, &saSigTerm, NULL );

	struct sigaction saSigHup;
	saSigHup.sa_handler = sigHupHandler;
	saSigHup.sa_flags = 0;
	sigemptyset(&saSigHup.sa_mask);
	sigaction(SIGHUP, &saSigHup, NULL );

	// Read configurations here.
	// For time being hardcoded file name and sleep interval.
	sleepTime = 2;

	// Open file and log time in it.
	int fd = open("/tmp/timelog", O_CREAT | O_APPEND | O_WRONLY, S_IRUSR | S_IWUSR);
	if (-1 == fd) {
		exit(EXIT_FAILURE);
	}

    //----------------
    //Main Process
    //----------------
    while (!bExit) {
        logTime(fd);	// Do the functionality of daemon.
    }

	return 0;
}

