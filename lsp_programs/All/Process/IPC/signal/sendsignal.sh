
#!/bin/bash

if [ ! $# -eq 3 ];then
	echo "Usage: $0 process_name signal_number iterations";
	exit 1;
fi

pid=`pgrep $1`

if [ -z $pid ];then
	echo "$1: No such process is running....";
	exit 1;
fi

for ((i=0; i<$3; i++));
do
	kill -$2 $pid
done

exit 0

