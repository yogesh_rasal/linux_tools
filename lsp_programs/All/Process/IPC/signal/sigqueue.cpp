
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void SigTermHandler(int iSigNum, siginfo_t* pSigInfo, void* pUContext)
{
	printf("In sigterm handler payload value = [%d]\n", pSigInfo->si_int);
}

int main()
{
	union sigval SigValue;
	struct sigaction SigTermAction;

	memset(&SigTermAction, 0, sizeof(SigTermAction));
	SigTermAction.sa_sigaction = SigTermHandler;
	SigTermAction.sa_flags = SA_SIGINFO;
	sigemptyset(&SigTermAction.sa_mask);

	if (0 != sigaction(SIGTERM, &SigTermAction, NULL))
	{
		printf("sigaction for SIGTERM failed for error [%s]\n", strerror(errno));
		return -1;
	}

	SigValue.sival_int = 100;
	if (0 != sigqueue(getpid(), SIGTERM, SigValue))
	{
		printf("sigqueue failed for error [%s]\n", strerror(errno));
		return -1;
	}

	printf("After sending signal SIGTERM using sigqueue\n");

	return 0;
}

