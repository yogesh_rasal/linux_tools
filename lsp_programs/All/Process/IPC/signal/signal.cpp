
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void SigHandler(int iSigNum)
{
	printf("Caught signal %d by process %d\n", iSigNum, getpid());
	//printf("Caught signal: %s\n", sys_siglist[iSigNum]);
	//psignal(iSigNum, "Caught signal: ");
	//printf("Caught signal: %s\n", strsignal(iSigNum));
}

int main()
{
	if (SIG_ERR == signal(SIGINT, SigHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	if (fork() != 0)
	{
		printf ("Parent process id = %d\n", getpid());
		for (;;)
		{
			printf("Parent::Before pause\n");
			pause();
			printf("Parent::After pause\n");
		}
	}
	else
	{
		printf ("child process id = %d\n", getpid());
		for (;;)
		{
			printf("Child::Before pause\n");
			pause();
			printf("Child::After pause\n");
		}
	}

	return 0;
}

