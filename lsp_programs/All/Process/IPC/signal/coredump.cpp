
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

int fun(int *ptr) {
	printf("Trying to access ptr\n");
	int val = *ptr;
	printf("ptr pointing to value %d\n", val);
	int *p = (int*)malloc(10 * sizeof(int));
	return val;
}

int main() {
	int i = 10;
	printf("First call returns: %d\n", fun(&i));
	printf("First call returns: %d\n", fun(NULL));

	return 0;
}
