
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void SigTermHandler(int iSigNum, siginfo_t* pSigInfo, void* pUContext)
{
	printf("In sigterm handler before sleep.\n");
	sleep(10);
	printf("In sigterm handler after sleep\n");
}

void SigIntHandler(int iSigNum)
{
	printf("In sigint handler before sleep\n");
	sleep(10);
	printf("In sigint handler after sleep\n");
}

int main()
{
	sigset_t SigSetToMask;
	struct sigaction SigIntAction;
	struct sigaction SigTermAction;


	//	During SIGINT mask all other signals.
	//	So while handling SIGINT, no other signal will be received.
	//	But while handling SIGTERM signal, other signals can be received.
    
    int pid;

    pid = getpid();

	printf("PID %d ",pid);

	sigemptyset(&SigSetToMask);
	sigfillset(&SigSetToMask);
	sigdelset(&SigSetToMask, SIGINT);

	memset(&SigIntAction, 0, sizeof(SigIntAction));
	SigIntAction.sa_handler = SigIntHandler;
	SigIntAction.sa_mask = SigSetToMask;

	if (0 != sigaction(SIGINT, &SigIntAction, NULL))
	{
		printf("sigaction  for SIGINT failed for error [%s]\n", strerror(errno));
		return -1;
	}

	memset(&SigTermAction, 0, sizeof(SigTermAction));
	SigTermAction.sa_sigaction = SigTermHandler;
	SigTermAction.sa_flags = SA_SIGINFO;
	sigemptyset(&SigTermAction.sa_mask);

	if (0 != sigaction(SIGTERM, &SigTermAction, NULL))
	{
		printf("sigaction for SIGTERM failed for error [%s]\n", strerror(errno));
		return -1;
	}

	for (;;)
	{
		printf("Before pause\n");
		pause();
		printf("After pause\n");
	}

	return 0;
}

