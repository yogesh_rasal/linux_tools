
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

static int got_signal = 0;

void SigHandler(int iSigNum)
{
	printf("Caught signal: %s\n", sys_siglist[iSigNum]);
	got_signal = 1;
}

int main()
{
	sigset_t SigMask;
	sigset_t OrigMask;

	//
	//	No need to register signal. U can block them even though they are not registered.
	//
	if (SIG_ERR == signal(SIGINT, SigHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	sigemptyset(&SigMask);
	sigaddset(&SigMask, SIGINT);
	sigaddset(&SigMask, SIGTERM);

	sleep(2);

	printf ("Setting sigprocmask for SIGINT\n");
	if (0 != sigprocmask(SIG_BLOCK, &SigMask, &OrigMask))
	{
		printf("Error in settings sigprocmask. Error: [%s]\n", strerror(errno));
		return -1;
	}
	else
	{
		printf("sigprocmask is successful.\n");
	}

	//
	//	Critical section task.
	//
	printf("Entering in critical section\n");
	sleep(15);
	printf("Out of critical section\n");

	printf ("Setting sigprocmask to origin\n");
	if (0 != sigprocmask(SIG_SETMASK, &OrigMask, NULL))
	{
		printf("Error in settings original sigprocmask. Error: [%s]\n", strerror(errno));
		return -1;
	}
	else
	{
		printf("sigprocmask is successful.\n");
	}

	sleep(5);

	if (got_signal)
	{
		printf("Signal is received\n");
	}

	return 0;
}

