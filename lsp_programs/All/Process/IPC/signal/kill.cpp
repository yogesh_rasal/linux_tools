
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

void SigHandler(int iSigNum)
{
	printf("Caught signal: [%s] by process [%d]\n", sys_siglist[iSigNum], getpid());
}

int main()
{
	pid_t ParentPid;
	pid_t ChildPid;

	if (SIG_ERR == signal(SIGINT, SigHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	ParentPid = getpid();

	ChildPid = fork();
	if (ChildPid == -1)
	{
		printf("fork failed for error [%s]\n", strerror(errno));
		return -1;
	}
	else if (ChildPid == 0)
	{
		printf ("child process id = %d\n", getpid());
		for (;;)
		{
			printf("Child::Before pause\n");
			pause();
			printf("Child::After pause\n");
		}
	}
	else
	{
		printf ("Parent process id = %d\n", getpid());
		sleep(5);

		printf("\n\n");
		printf("Checking whether we can send signal to child process using kill with signo=0.\n");
		if (0 != kill(ChildPid, 0))
		{
			printf("We cannot send signal to child. Error: [%s]\n", strerror(errno));
		}
		else
		{
			printf("We can send signal to child.\n");
		}
		sleep(5);

		printf("\n\n");
		printf("Sending SIGINT using kill with pid=childpid\n");
		if (0 != kill(ChildPid, SIGINT))
		{
			printf("Sending SIGINT failed for error [%s]\n", strerror(errno));
		}
		else
		{
			printf("Sending SIGINT success.\n");
		}
		sleep(5);

		printf("\n\n");
		printf("Sending SIGINT using kill with pid=0\n");
		if (0 != kill(0, SIGINT))
		{
			printf("Sending SIGINT failed for error [%s]\n", strerror(errno));
		}
		else
		{
			printf("Sending SIGINT success.\n");
		}
		sleep(5);

		//
		//	WARNING:: DONT UNCOMMENT AND RUN FOLLOWING CODE.
		//	IT WILL KILL ALL PROCESSES IF U RUN IT WITH SUPER USER PRIVILEDGES.
		//
		/*printf("\n\n");
		printf("Sending SIGINT using kill with pid=-1\n");
		if (0 != kill(-1, SIGINT))
		{
			printf("Sending SIGINT failed for error [%s]\n", strerror(errno));
		}
		else
		{
			printf("Sending SIGINT success.\n");
		}
		sleep(5);*/

		printf("\n\n");
		printf("Sending SIGINT to self using raise system call\n");
		if (0 != raise(SIGINT))
		{
			printf("Sending SIGINT to self failed for error [%s]\n", strerror(errno));
		}
		else
		{
			printf("Sending SIGINT to self success.\n");
		}
		printf("After sending SIGINT to self using raise system call\n");
		sleep(5);

		printf("Sending SIGTERM to child....\n");
		kill(ChildPid, SIGTERM);
		sleep(5);
	}

	return 0;
}

