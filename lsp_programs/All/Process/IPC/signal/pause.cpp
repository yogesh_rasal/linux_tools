
#include <signal.h>
#include <unistd.h>
#include <stdio.h>

void SigHandler(int iSigNum)
{
	printf("Caught signal %d\n", iSigNum);
}

int main()
{
	if (SIG_ERR == signal(SIGINT, SigHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	for (;;)
	{
		printf("Before pause\n");
		pause();
		printf("After pause\n");
	}

	return 0;
}

