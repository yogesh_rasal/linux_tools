
Process can behave in following different ways when it is delivered a signal:
	- Terminate
	- Ignore
	- Terminate and dump core
	- Stop
	- Continue if already stopped

Signal handler: A programmer defined function that is invoked automatically when signal is delivered.

Signal handler is invoked on normal process stack. Alternate stack can be set for signal handler using function sigaltstack.

The signal disposition is a per process attribute. In a multi-threaded application the disposition of a particular
signal is same for all threads.

A child created via fork inherits a copy of its patent's signal dispositions. During an execve, the dispositions of
handled signals are reset to defaults, the disposition of ignored signals are left unchanged.

ulimit -c unlimited   // command for setting the coredump for c

gdb ./a.out core      // to check the dump with gdb


