
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

//
//	While running this module, send different signals while process is busy in handling another signal. The signal is
//	received and handled by process in another signal handler.
//	To mask signals while handling one signal, use sigaction. Check sigaction.cpp for more details.
//
void SigTermHandler(int iSigNum)
{
	printf("In sigterm handler before sleep\n");
	sleep(10);
	printf("In sigterm handler after sleep\n");
}

void SigIntHandler(int iSigNum)
{
	printf("In sigint handler before sleep\n");
	sleep(10);
	printf("In sigint handler after sleep\n");
}

int main()
{
	printf("pid: %d\n", getpid());
	if (SIG_ERR == signal(SIGINT, SigIntHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	if (SIG_ERR == signal(SIGTERM, SigTermHandler))
	{
		printf ("Error in signal registration\n");
		return -1;
	}

	for (;;)
	{
		printf("Before pause\n");
		pause();
		printf("After pause\n");
	}

	return 0;
}

