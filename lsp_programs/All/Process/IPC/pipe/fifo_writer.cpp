#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv)
{
    int fd;
	int ret;
    char myfifo[] = "./myfifo";

    /* create the FIFO (named pipe) */
    mkfifo(myfifo, 0666);
	signal(SIGPIPE, SIG_IGN);

	printf("Before opening fifo for writing\n");
	// Give any command line argument to open file in non blocking mode.
	if (argc == 1) {
		fd = open(myfifo, O_WRONLY);
	} else {
		fd = open(myfifo, O_WRONLY | O_NDELAY);
	}
	if (-1 == fd) {
		printf("Fifo open failed. Error: %s\n", strerror(errno));
		return -1;
	}
	printf("Fifo opened succesfully\n");

	for (int i = 1; i <= 5; i++) {
		printf("Writer writing in iteration %d\n", i);
		ret = write(fd, "Hi\n", strlen("Hi\n"));
		printf("Return value %d\n", ret);
		if (-1 == ret) { 
			printf("Write failed for error: %s\n", strerror(errno));
			close(fd);
			return -1;
		}
		sleep(1);
	}
    close(fd);

    /* remove the FIFO */
    unlink(myfifo);

    return 0;
}

