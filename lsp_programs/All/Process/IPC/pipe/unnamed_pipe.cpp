#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main(void)
{
	int fd[2], nbytes;
	pid_t childpid;
	char string[] = "Hello, world!\n";
	char readbuffer[1024];

	pipe(fd);
	childpid = fork();
	if(childpid == -1)
	{
		perror("fork");
		exit(1);
	} else if(childpid == 0) {
		/* Child process closes up input side of pipe */
		close(fd[0]);

		/* Send "string" through the output side of pipe */
		for (int i = 0; i < 5; i++)
			write(fd[1], string, strlen(string));
		close(fd[1]);
		exit(0);
	} else {
		/* Parent process closes up output side of pipe */
		close(fd[1]);

		/* Read in a string from the pipe */
		nbytes = read(fd[0], readbuffer, sizeof(readbuffer));
		printf("Received string: %s\n", readbuffer);
		close(fd[0]);
	}

	return 0;
}

