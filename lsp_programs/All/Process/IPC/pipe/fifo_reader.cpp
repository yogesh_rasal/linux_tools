#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#define MAX_BUF 1024

int main(int argc, char **argv)
{
    int fd;
	int ret;
    char myfifo[] = "./myfifo";
    char buf[MAX_BUF];

	printf("Before opening fifo\n");
    /* open, read, and display the message from the FIFO */
	// Give any command line argument to open file in non blocking mode.
	if (argc == 1) {
		fd = open(myfifo, O_RDONLY);
	} else {
		fd = open(myfifo, O_RDONLY | O_NONBLOCK);
	}
	if (-1 == fd) {
		printf("Fifo open failed. Error: %s\n", strerror(errno));
		return -1;
	}
	printf("Fifo opened succesfully\n");

    ret = read(fd, buf, MAX_BUF);
	if (-1 == ret) { 
		printf("Read failed for error: %s\n", strerror(errno));
		close(fd);
		return -1;
	}
    printf("Read successful. Characters read = %d, data: %s\n", ret, buf);
    close(fd);

    return 0;
}

