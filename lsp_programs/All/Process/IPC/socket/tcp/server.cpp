#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>

bool bExit = false;

void sigIntHandler(int signum) {
	printf("In sigInt Handler\n");
	bExit = true;
}

int main()
{
	int ret;
	int serverSocket, clientSocket;
	char buffer[1024];
	struct sockaddr_in serverAddr;

	signal(SIGINT, sigIntHandler);
	/*struct sigaction a;
	a.sa_handler = sigIntHandler;
	a.sa_flags = 0;
	sigemptyset( &a.sa_mask );
	sigaction( SIGINT, &a, NULL );*/

	/*---- Create the socket. The three arguments are: ----*/
	serverSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (-1 == serverSocket) {
		printf("Socket creation failed. Error: %s\n", strerror(errno));
		return -1;
	}

	/*---- Configure settings of the server address struct ----*/
	/* Address family = Internet */
	serverAddr.sin_family = AF_INET;
	/* Set port number, using htons function to use proper byte order */
	serverAddr.sin_port = htons(7891);
	/* Set IP address to localhost */
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	/*---- Bind the address struct to the socket ----*/
	ret = bind(serverSocket, (struct sockaddr *) &serverAddr, sizeof(serverAddr));
	if (-1 == ret) {
		printf("Socket bind failed. Error: %s\n", strerror(errno));
		close(serverSocket);
		return -1;
	}

	/*---- Listen on the socket, with 5 max connection requests queued ----*/
	ret = listen(serverSocket, 5);
	if (-1 == ret) {
		printf("Socket listen failed. Error: %s\n", strerror(errno));
		close(serverSocket);
		return -1;
	}

	/*---- Accept call creates a new socket for the incoming connection ----*/
	while (!bExit) {
		clientSocket = accept(serverSocket, NULL, NULL);
		if (-1 == clientSocket) {
			printf("Accept failed. Error: %s\n", strerror(errno));
			continue;
		}
		printf("Accepted connection. Client Socket: %d\n", clientSocket);

		/*---- Send message to the socket of the incoming connection ----*/
		strcpy(buffer,"Hello World\n");
		send(clientSocket, buffer, 13, 0);
	}

	close(serverSocket);

	return 0;
}

