#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

int main()
{
	int ret;
	int clientSocket;
	char buffer[1024];
	struct sockaddr_in serverAddr;
	socklen_t addr_size;

	/*---- Create the socket. The three arguments are: ----*/
	/* 1) Internet domain 2) Stream socket 3) Default protocol (TCP in this case) */
	clientSocket = socket(PF_INET, SOCK_STREAM, 0);
	if (-1 == clientSocket) {
		printf("Socket creation failed. Error: %s\n", strerror(errno));
		return -1;
	}

	/*---- Configure settings of the server address struct ----*/
	/* Address family = Internet */
	serverAddr.sin_family = AF_INET;
	/* Set port number, using htons function to use proper byte order */
	serverAddr.sin_port = htons(7891);
	/* Set IP address to localhost */
	serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

	/*---- Connect the socket to the server using the address struct ----*/
	ret = connect(clientSocket, (struct sockaddr *) &serverAddr, sizeof serverAddr);
	if (-1 == ret){
		printf("Connect failed for error %s\n", strerror(errno));
		close(clientSocket);
		return -1;
	}

	/*---- Read the message from the server into the buffer ----*/
	recv(clientSocket, buffer, 1024, 0);

	/*---- Print the received message ----*/
	printf("Data received: %s",buffer);   

	return 0;
}

