
#ifndef	_MSGQ_H_
#define _MSGQ_H_


#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/types.h>


#define MESSAGE_Q_ID				1000


struct Message
{
	long lMsgType;
	char szData[6];
};


#endif	//	_MSGQ_H_

