#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<mqueue.h>

int main()
{

	mqd_t mqdes;
	mqd_t ret_value;
	mqd_t ret_attr;
	char buffer[20];
	struct mq_attr attr;

	mqdes = mq_open("/posixmessage", O_RDONLY, 0666);
	if(-1 == mqdes)
	{
		printf("Error: Posix Message Queue open failed: %s\n", strerror(errno));
		return -1;
	}
	printf("Posix message queue open success\n");
	
	ret_attr = mq_getattr(mqdes, &attr);
	if(-1 == ret_attr)
	{
		printf("Error : Posix message queue get attribute failed: %s\n", strerror(errno));
		mq_close(mqdes);
		return -1;
	}
	
	ret_value = mq_receive(mqdes, buffer, attr.mq_msgsize, NULL);
	if(-1 == ret_value)
	{
		printf("Error: Message read failed : %s\n",strerror(errno));
		mq_close(mqdes);
		return -1;
	}
	printf("Message read success\nData = %s\n", buffer);
	mq_close(mqdes);
	return 0;
	
}
