#include<stdio.h>
#include<string.h>
#include<unistd.h>
#include<errno.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<mqueue.h>

int main()
{

	mqd_t mqdes;
	mqd_t ret_value;
	struct mq_attr attr;
	char buffer[20] = "Hello World!";

	attr.mq_maxmsg = 10;
	attr.mq_msgsize = strlen(buffer);
	attr.mq_flags = 0;

	mqdes = mq_open("/posixmessage", O_CREAT | O_RDWR, 0666, &attr);
	if(-1 == mqdes)
	{
		printf("Error : Posix message queue open failed : %s\n", strerror(errno));
		return -1;
	}
	printf("Posix Message Queue open success\n");
	printf("buffer length = %d\n", strlen(buffer));

	ret_value = mq_send(mqdes, buffer, strlen(buffer), 1);
	if(-1 == ret_value)
	{
		printf("Error : Writing data to queue failed : %s\n", strerror(errno));
		mq_close(mqdes);
		return -1;
	}
	printf("Posix message queue write success\n");
	sleep(20);
	close(mqdes);
	mq_unlink("/posixmessage");
	return 0;
}
