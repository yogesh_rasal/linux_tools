#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>

int main(int argc,char **argv)
{
	int ret;
	int fd;
	char buffer[1024];

	if(argc!=2)
	{
		printf("usage : %s <file_path>\n",argv[0]);
		exit(-1);
	}

	printf("UID: %u, EUID: %u\n", getuid(), geteuid());
	fd = open(argv[1], O_RDONLY);
	if (-1 == fd)
	{
		printf("file open failed. Error = %s \n",strerror(errno));
		return -1;
	}

	memset(buffer, 0, sizeof(buffer));
	ret = read(fd, buffer, sizeof(buffer));
	if(-1 == ret)
	{
		printf("read failed. Error = %s \n",strerror(errno));
		close(fd);
		return -1;
	}

	printf("Data read: %s\n", buffer);
	close(fd);
	return 0;
}

