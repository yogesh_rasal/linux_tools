#include <iostream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

using namespace std;

int main()
{
	int iret;
	int shmid;
	key_t shmkey;
	void *shmaddr;

	//
	//	Create key.
	//
	shmkey = ftok("./sharedmem.shm", 'S');
	if (-1 == shmkey)
	{
		cout << "ftok failed for error " << strerror(errno) << "\n";
		return -1;
	}
	cout << "Shared memory key =" << shmkey << "\n";

	//
	//	Create shared memory.
	//
	shmid = shmget(shmkey, 1024, IPC_PRIVATE);
	if (-1 == shmid)
	{
		cout << "msgget failed for error " << strerror(errno) << "\n";
		return -1;
	}
	cout << "Shmget success\n";

	//
	//	Attach shared memory.
	//
	shmaddr = shmat(shmid, NULL, 0);
	if (shmaddr == (void*)-1)
	{
		cout << "shmaddr failed for error " << strerror(errno) << "\n";
		shmctl(shmid, IPC_RMID, NULL);
		return -1;
	}
	cout << "Shmat success\n";

	cout << "Initial data in segment = " << (char*)shmaddr << "\n";
	sleep(15);
	cout << "changed data in segment = " << (char*)shmaddr << "\n";

	shmdt(shmaddr);

	return 0;
}

