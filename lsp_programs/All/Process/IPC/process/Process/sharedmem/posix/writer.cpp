#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <errno.h>           /* For O_* constants */
#include <string.h>           /* For O_* constants */
#include <stdio.h>           /* For O_* constants */
#include <unistd.h>

int main()
{
	int iFD;
	int iRet;

	iFD = shm_open("/sharedmem", O_CREAT | O_RDWR, 0666);
	if (-1 == iFD)
	{
		printf("shm_open failed for error %s\n", strerror(errno));
		return -1;
	}
	printf("shm_open success\n");

	iRet = write(iFD, "SB01", 4);
	if (-1 == iRet)
	{
		printf("write failed for error %s\n", strerror(errno));
		close(iFD);
		return -1;
	}
	printf("shm_write success\n");

	sleep(15);

	close(iFD);

	shm_unlink("/sharedmem");

	return 0;
}

