#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include<sys/stat.h>
#include<sys/types.h>
#include<errno.h>

int a = 20;

int main(int argc, char** argv)
{
	int b = 10;
	int pid;

	pid = fork();

	if(-1 == pid)
	{
		printf("Error occurred while creating a child process\n");
		return 0;
	}
	else if(0 == pid)
	{
		printf("Child process : %d\n", pid);
		b = 15;
	}
	else if(pid > 0)
	{
		a = 25;
		printf("Parent process with child pid : %d\n", pid);
	}
	printf("a = %d, b = %d\n", a, b);
}
