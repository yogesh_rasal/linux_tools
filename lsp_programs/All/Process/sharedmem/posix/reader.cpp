#include <sys/mman.h>
#include <sys/stat.h>        /* For mode constants */
#include <fcntl.h>           /* For O_* constants */
#include <errno.h>           /* For O_* constants */
#include <string.h>           /* For O_* constants */
#include <stdio.h>           /* For O_* constants */
#include <unistd.h>

int main()
{
	int iFD;
	int iRet;

	iFD = shm_open("/sharedmem", O_RDONLY, 0666);
	if (-1 == iFD)
	{
		printf("shm_open failed for error %s\n", strerror(errno));
		return -1;
	}
	printf("shm_open success\n");

	char szBuf[20] = {0};
	iRet = read(iFD, szBuf, 4);
	if (-1 == iRet)
	{
		printf("read failed for error %s\n", strerror(errno));
		close(iFD);
		return -1;
	}
	printf("Read success. data = %s\n", szBuf);

	//sleep(15);

	close(iFD);

	//shm_unlink("/sharedmem");

	return 0;
}

