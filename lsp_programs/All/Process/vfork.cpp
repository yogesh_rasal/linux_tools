#include <stdio.h>  
#include <unistd.h>
#include <stdlib.h>

int a = 20;
int main()  
{  
	pid_t pid;  
	pid = vfork();  
	if(pid == 0)  
	{  
		printf("Child is going to sleep\n");  
		sleep(5);
		printf("Child is wakeup\n");
		a = 30;
		exit(0);
		//return 0;
	}

	printf("Parent is going to sleep\n");  
	sleep(5);
	printf("Parent is wakeup\n");
	printf("Parent a = %d\n", a);

	return 0;  
}

