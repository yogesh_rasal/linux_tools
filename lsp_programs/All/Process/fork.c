#include<stdio.h>
#include<unistd.h>

int a = 20;

int main()
{
    int ipid;    
    int ib = 25;    
    ipid = fork();
    if( -1 == ipid )
    {
        printf("Fork failed\n");
        return 0;
    }
    
    if( 0 == ipid )
    {
        printf("child  proces a= %d\n",a);
        sleep(1);
        ib = 15;
    }
    else
    {
        printf("parent proces a= %d\n",a);
        a = 30;
        ib = 35;
        sleep(1);
    }


    printf(" ipid = %5d : a = %d\n",ipid,a);
    printf(" ipid = %5d : b = %d\n",ipid,ib);

    return 0;
}


