
#include "msgq.h"


int
main(
	)
{
	int iRet;
	int iMQId;
	struct Message msg;

	//
	//	Create message queue.
	//
	iMQId = msgget(MESSAGE_Q_ID, 0);
	if (-1 == iMQId)
	{
		printf("Message queue open failed for error %s\n", strerror(errno));
		return -1;
	}

	printf("Message queue opened successfully.\n");

	//
	//	Receive message1.
	//
	iRet = msgrcv(iMQId, &msg, 4, 2l, IPC_NOWAIT);
	if (-1 == iRet)
	{
		printf("Message1 receiving failed for error %s\n", strerror(errno));
		msgctl(iMQId, IPC_RMID, NULL);
		return -1;
	}

	printf("Message1 id = %ld, Text = (%s)\n", msg.lMsgType, msg.szData);

	//
	//	Receive message2.
	//
	iRet = msgrcv(iMQId, &msg, 6, 1l, 0);
	if (-1 == iRet)
	{
		printf("Message2 receiving failed for error %s\n", strerror(errno));
		msgctl(iMQId, IPC_RMID, NULL);
		return -1;
	}

	printf("Message2 id = %ld, Text = (%s)\n", msg.lMsgType, msg.szData);

	//
	//	Remove message queue.
	//
	//sleep(20);
	//msgctl(iMQId, IPC_RMID, NULL);

	return 0;
}

