
#include "msgq.h"


int
main(
	)
{
	int iRet;
	int iMQId;
	struct Message msg;

	//
	//	Create message queue.
	//
	iMQId = msgget(MESSAGE_Q_ID, IPC_CREAT | S_IRUSR | S_IWUSR);
	if (-1 == iMQId)
	{
		printf("Message queue creation failed for error %s\n", strerror(errno));
		return -1;
	}

	printf("Message queue created successfully.\n");

	msg.lMsgType = 1l;
	strcpy(msg.szData, "hello");

	//
	//	Send message.
	//
	iRet = msgsnd(iMQId, &msg, 6, IPC_NOWAIT);
	if (0 != iRet)
	{
		printf("Message1 sending failed for error %s\n", strerror(errno));
		msgctl(iMQId, IPC_RMID, NULL);
		return -1;
	}

	printf("Message1 sent successfully.\n");

	msg.lMsgType = 2l;
	strcpy(msg.szData, "world");

	//
	//	Send message.
	//
	iRet = msgsnd(iMQId, &msg, 6, IPC_NOWAIT);
	if (0 != iRet)
	{
		printf("Message2 sending failed for error %s\n", strerror(errno));
		msgctl(iMQId, IPC_RMID, NULL);
		return -1;
	}

	printf("Message2 sent successfully.\n");

	//
	//	Remove message queue.
	//
	printf("Sleeping before deleting message queue\n");
	sleep(20);
	msgctl(iMQId, IPC_RMID, NULL);

	return 0;
}

