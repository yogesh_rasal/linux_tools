#include <stdio.h>  
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()  
{  
	pid_t pid1, pid2;
	int status, ret;

	pid1 = fork();
	if(pid1 == 0)
	{  
		printf("Child1 having pid: %d is going to sleep\n", getpid());  
		sleep(10);
		printf("Child1 is wakeup and will exit with status 1\n");
		exit(1);
	}

	pid2 = fork();
	if(pid2 == 0)
	{  
		printf("Child2 having pid: %d is going to sleep\n", getpid());  
		sleep(5);
		printf("Child2 is wakeup and will exit with status 2\n");
		exit(2);
	}

	printf("Parent- Child1 pid: %u, Child2 pid: %u\n", pid1, pid2);
	ret = wait(&status);
	printf("wait() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));
	ret = wait(&status);
	printf("wait() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));
	ret = wait(&status);
	printf("wait() returned. Retvalue: %d, status: %d\n", ret, WEXITSTATUS(status));

	return 0;  
}

