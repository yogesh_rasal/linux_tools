#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>

int main(int argc,char **argv)
{
	printf("Exec process pid = %u\n", getpid());

	int ret;
	// This program is executed by "exec" system call by o_cloexec program.
	// Try to write data in using file descriptors 3 and 4. See o_cloexec.cpp for more understanding.
	char NoCloseExec[] = "NoCloseExecAnkit";
	ret = write(3, NoCloseExec, strlen(NoCloseExec));
	if (ret < 0) {
		printf ("Write failed for file descriptor 3\n");
		return -1;
	}
	printf("Write succesful using file descriptor 3\n");

	char CloseExec[] = "CloseExec";
	ret = write(4, CloseExec, strlen(CloseExec));
	if (ret < 0) {
		printf ("Write failed for file descriptor 4\n");
		return -1;
	}
	printf("Write succesful using file descriptor 4\n");

	close(3);

	return 0;
}

