#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<fcntl.h>
#include<errno.h>

int main(int argc,char **argv)
{
	pid_t pid;
	int ret;
	int fdCloexec;
	int fdNoCloexec;

	if(argc!=4)
	{
		printf("usage : %s <file_path1> <file_path2> <program_path_to_exec>\n",argv[0]);
		exit(-1);
	}

	// Open file without O_CLOEXEC flag.
	fdNoCloexec = open(argv[1], O_WRONLY | O_APPEND);
	if (-1 == fdNoCloexec)
	{
		printf("file %s open failed. Error = %s \n", argv[1], strerror(errno));
		return -1;
	}
	printf("fdNoCloexec fd = %d\n", fdNoCloexec);

	// Open file with O_CLOEXEC flag.
	fdCloexec = open(argv[2], O_WRONLY | O_APPEND | O_CLOEXEC);
	if (-1 == fdCloexec)
	{
		printf("file %s open failed. Error = %s \n", argv[2], strerror(errno));
		close(fdNoCloexec);
		return -1;
	}
	printf("fdCloexec fd = %d\n", fdCloexec);

	// Write data in both files.
	char NoCloseExec[] = "NoCloseExec";
	ret = write(fdNoCloexec, NoCloseExec, strlen(NoCloseExec));

	char CloseExec[] = "CloseExec";
	ret = write(fdCloexec, CloseExec, strlen(CloseExec));

	// Create new process.
    pid = fork();
    if (-1 == pid)
    {
        printf("fork() failed for error = %s \n", strerror(errno));
        close(fdNoCloexec);
        close(fdCloexec);
        return -1;
    } 
    else if (0 == pid) 
    {
        printf("Child pid = %u\n", getpid());
        // Exec new process. Successful exec never returns.
        ret = execl(argv[3], argv[3], (char*)0);
        if (-1 == ret) 
        {
            close(fdNoCloexec);
            close(fdCloexec);
            return -1;
        }
    } 
    else 
    {
        // Parent will execute this code.
        close(fdNoCloexec);
        close(fdCloexec);
    }

    return 0;
}

